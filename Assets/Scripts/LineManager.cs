using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.XR.Interaction.Toolkit.AR;
using TMPro;
using System;

public class LineManager : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public ARPlacementInteractable placementInteracable;
    public TextMeshPro mText;
    private int pointCount = 0;
    LineRenderer line;
    public bool continuous;
    public TextMeshProUGUI buttonText;

    // Start is called before the first frame update
    // inisiasi DrawLine
    void Start()
    {
        placementInteracable.objectPlaced.AddListener(DrawLine);
    }

    // ganti line terpisah dan sambung
    public void ToogleBetweenDiscreteAndContinuous()
    {
        continuous = !continuous;

        if (!continuous)
        {
            buttonText.text = "Terpisah";
        }   
        else
        {
            buttonText.text = "Sambung";
        }
            
    }

    // reset area pengukuran dengan me-refresh scene
    public void ResetLine()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void DrawLine(ARObjectPlacementEventArgs args)
    {
        pointCount++; // menambah titik pengukuran

        // setelah titik muncul, 
        // maka akan menghasilkan garis di titik berikutnya
        if (pointCount < 2)
        {
            line = Instantiate(lineRenderer);
            line.positionCount = 1;
        } 
        else
        {
            line.positionCount = pointCount;
            // titik akan reset dari 0 untuk garis terpisah
            if (!continuous)
                pointCount = 0;
        }
        
        // set lokasi point di line renderer
        line.SetPosition(line.positionCount-1, args.placementObject.transform.position);

        // garis pengukuran sudah muncul
        if (line.positionCount > 1)
        {
            Vector3 pointA = line.GetPosition(line.positionCount - 1); // titik A
            Vector3 pointB = line.GetPosition(line.positionCount - 2); // titik B
            float distance = Vector3.Distance(pointA, pointB); // jarak A ke B

            // mencetak hasil pengukuran
            TextMeshPro distText = Instantiate(mText);
            distText.text = "" + Math.Round(distance, 2) + " m";

            // transformasi arah garis pengukuran
            Vector3 directionVector = pointB - pointA;
            Vector3 normal = args.placementObject.transform.up;

            Vector3 upd = Vector3.Cross(directionVector, normal).normalized;
            Quaternion rotation = Quaternion.LookRotation(-normal, upd);

            // meletakkan hasil pengukuran di atas garis
            distText.transform.rotation = rotation;
            distText.transform.position = (pointA + directionVector * 0.5f) + upd * 0.05f;
        }
    }
    
    // pindah ke scene menu utama
    public void MoveToMain()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
