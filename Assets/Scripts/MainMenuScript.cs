using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public void MoveToMeteran()
    {
        SceneManager.LoadScene("ARMeteran");
    }

    public void QuitApp()
    {
        Application.Quit();
    }
}
