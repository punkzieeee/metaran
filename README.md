# MetARan


## Getting started

This project belongs to my friend who already graduated this year (Apr 2023). With his permission, I decided to put this project on Gitlab since he doesn't have one.

## About

MetARan is a simple app that helps user to measure any object with augmented reality. This app can only measure length and width.

## Minimum Requirement

This app designed for Android OS with at least 8.1 version.

## How To Use

1. Make sure your phone facing flat surface.
2. If the yellow area appeared, you can start the measurement.
3. Touch the yellow area on your screen to see the result.

There are three buttons that appear during measurement:
- "Sambung/Terpisah" (left): change the measurement mode (continuous/separated).
- "Reset" (middle): refresh the screen and remove last measurement.
- "Main Menu" (right): back to main menu. self explanatory.

## Latest Version

If you want to try for yourself, please download version 2 (**v2**) in folder "**Apk**"
